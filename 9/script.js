const getNumbercard = () => {
  let input = document.createElement('input');
  let button = document.createElement('button');
  let cardsContainer = document.querySelector('.header');

  input.placeholder = 'Введите четное количество карточек от 2 до 16';
  button.innerHTML = 'Начать игру';

  input.classList.add('form-control');
  button.classList.add('btn', 'btn-primary');
  cardsContainer.prepend(input);
  cardsContainer.prepend(button);
  return {
    input, 
    button
  };
};

function main() {
  function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
};

let arrNumber = shuffle([1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8]);
console.log(arrNumber);

let card = document.querySelectorAll(".card-container");
let stroke = document.querySelectorAll(".text-info");
let arr = [];
let stroreText;
function assignNumber() {
  for (let i = 0; i <= card.length-1; i++) {
    card[i].addEventListener("click", function() {
      stroke[i].textContent = arrNumber[i];
      let obj = {};
      card[i].id = 'id';
      stroreText = stroke[i].textContent;
      obj.number =   stroke[i].textContent;
      obj.card = i;
      arr.push(obj);
      console.log(arr);
      clearButton();
      showButton();
      /*setTimeout(clearButton, 1000);*/
    });
  };
};
 assignNumber();

 function clearButton() {
  if (arr.length % 3 === 0) {
    if (arr[arr.length-3].number !== arr[arr.length-2].number) {
      stroke[arr[arr.length-3].card].textContent = "";
      stroke[arr[arr.length-2].card].textContent = "";
      card[arr[arr.length-3].card].removeAttribute('id');
      card[arr[arr.length-2].card].removeAttribute('id');
      arr.splice(arr.length-3,2);
    }else if (arr[arr.length-3].number === arr[arr.length-2].number) {
      arr.splice(arr.length-3,2);
    }else if (arr[arr.length-2].number === arr[arr.length-1].number) {
      stroke[arr[arr.length-3].card].textContent = "";
      arr.splice(arr.length-3,1);
    };
  };
};

function showButton() {
  let findID = document.querySelectorAll('#id');
  console.log(findID);
    if (findID.length === 16) {
      let button = document.createElement('button');
      let cardsContainer = document.querySelector('.cardsContainer');
      button.innerHTML = 'Сыграть еще раз';
      button.classList.add('btn', 'btn-primary');
      cardsContainer.append(button); 
      button.addEventListener('click', function(){
        location.reload();
      });
    };
  };
};



  //Рабочая функция 

  /*function main() {

    function shuffle(array) {
        for (let i = array.length - 1; i > 0; i--) {
          let j = Math.floor(Math.random() * (i + 1));
          [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    };
  
    let arrNumber = shuffle([1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8]);
    console.log(arrNumber);
  
    let card = document.querySelectorAll(".card-container");
    let stroke = document.querySelectorAll(".text-info");
    let arr = [];
    let stroreText;
    function assignNumber() {
      for (let i = 0; i <= card.length-1; i++) {
        card[i].addEventListener("click", function() {
          stroke[i].textContent = arrNumber[i];
          let obj = {};
          card[i].id = 'id';
          stroreText = stroke[i].textContent;
          obj.number =   stroke[i].textContent;
          obj.card = i;
          arr.push(obj);
          console.log(arr);
          clearButton();
          showButton();
          /*setTimeout(clearButton, 1000);*/
   /*     });
      };
    };
     assignNumber();*/
  
     /*function clearButton() {
      if (arr.length % 3 === 0) {
        if (arr[arr.length-3].number !== arr[arr.length-2].number) {
          stroke[arr[arr.length-3].card].textContent = "";
          stroke[arr[arr.length-2].card].textContent = "";
          card[arr[arr.length-3].card].removeAttribute('id');
          card[arr[arr.length-2].card].removeAttribute('id');
          arr.splice(arr.length-3,2);
        }else if (arr[arr.length-3].number === arr[arr.length-2].number) {
          arr.splice(arr.length-3,2);
        }else if (arr[arr.length-2].number === arr[arr.length-1].number) {
          stroke[arr[arr.length-3].card].textContent = "";
          arr.splice(arr.length-3,1);
        };
      };
    };
  
    function showButton() {
      let findID = document.querySelectorAll('#id');
      console.log(findID);
        if (findID.length === 16) {
          let button = document.createElement('button');
          let cardsContainer = document.querySelector('.cardsContainer');
          button.innerHTML = 'Сыграть еще раз';
          button.classList.add('btn', 'btn-primary');
          cardsContainer.append(button); 
          button.addEventListener('click', function(){
            location.reload();
          });
        };
      };
};*/
